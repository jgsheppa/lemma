import React from 'react';
import './App.css';

import Button from './components/button'
import Container from './components/pageContainer/';

import {ReactComponent as Logo} from './filebox.svg'

function App() {
  return (
    <Container>
      <Logo style={{background: "#1ca4b7", height: "4rem", width: "4rem"}}/>
      <Button target="_blank" href="moz-extension://4a773075-9377-c14c-9724-e7ed2101188b/index.html" maxWidth="12rem" backgroundColor="#5a58df" color="#ffffff" border="none" width="100%" borderRadius="2px" height="4rem">New Lemma</Button>
    </Container>
  );
}

export default App;
