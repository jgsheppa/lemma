import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  min-width: 5rem;
  max-width: 64rem;
  width: 100%;
  height: 100%;
  max-height: 14rem;
  padding: 2rem 4rem;
`;