import { Container } from './styles'
   
import React, { FunctionComponent } from "react";

const pageContainer: FunctionComponent = ({children}) => {
  return (
      <Container >{children}</Container>
  );
};


export default pageContainer;