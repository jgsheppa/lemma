import styled from 'styled-components'

import { LinkProps } from './types';

export const Link = styled.a<LinkProps>`
  text-align: ${(props) => props.textAlign || "center"};
  padding: ${(props) => props.padding || "auto"};
  font-weight: ${(props) => props.fontWeight || "initial"};
  margin: ${(props) => props.margin || "0.6rem 0.4em"};
  width: ${({width}) => width || "initial"};
  max-width: ${({maxWidth}) => maxWidth || "initial"};
  height: ${(props) => props.height || "initial"};
  border-radius: ${({borderRadius}) => borderRadius || "initial"};
  background-color: ${({backgroundColor}) => backgroundColor || "initial"};
  color: ${({color}) => color || "initial"};
  cursor: pointer;
`;