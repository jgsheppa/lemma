
   
import React, { FunctionComponent } from "react";

import { Link } from "./styles";
import { LinkProps } from "./types";

const CommonLink: FunctionComponent<LinkProps> = ({href, target, children}) => {
  return (
      <Link href={href} target={target} >{children}</Link>
  );
};


export default CommonLink;