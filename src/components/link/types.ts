export type LinkProps = {
  width?: string;
  maxWidth?: string;
  height?: string;
  borderRadius?: string;
  fontSize?: string;
  border?: string;
  backgroundColor?: string;
  textAlign?: string;
  padding?: string;
  fontWeight?: string;
  margin?: string;
  color?: string;
  onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
  href?: string;
  target?: string;
}